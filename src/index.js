import React from 'react';
import reactDom from "react-dom";
import {Provider} from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

import App from './components/app';
import ErrorBoundry from './components/error-boundry';
import BookstoreService from './services/bookstore-service';
import {BookstoreServiceProvider} from './components/bookstore-service-context';

import store from './store';

const Main = () => {
    const bookstoreService = new BookstoreService();

    return(
        <Provider store={store}>
            <ErrorBoundry>
                <BookstoreServiceProvider value={bookstoreService}>
                    <Router>
                        <App/>
                    </Router>
                </BookstoreServiceProvider>
            </ErrorBoundry>
        </Provider>
    )
}

reactDom.render(
    <Main/>,
    document.getElementById('root')
)