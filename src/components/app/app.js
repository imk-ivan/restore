import React from 'react';
import './app.css';
import { Route, Switch } from 'react-router';
import HomePage from '../pages/home-page';
import CartPage from '../pages/cart-page';
import ShopHeader from '../shop-header/shop-header';

function App() {
    return (
        <main role="main" className="app container">
            <ShopHeader itemsQuantity={3} total={84.50}/>
            <Switch>
                <Route path='/' exact component={HomePage}/>
                <Route path='/cart' component={CartPage}/>
            </Switch>
        </main>
    )
}

export default App;