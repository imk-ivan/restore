import React from 'react'
import BookItemsList from '../../book-items-list/book-items-list'
import ShoppingCartTable from '../../shopping-cart-table/shopping-cart-table'

function HomePage() {

    return (
        <div className="home-page">
            <div className="page-heading">
                <h1>Home Page</h1>
            </div>
            <BookItemsList />
            <ShoppingCartTable/>
        </div>
    )
}
export default HomePage
