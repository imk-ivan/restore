
import React from 'react'
import { Link } from 'react-router-dom';
import './shop-header.css';

function ShopHeader({itemsQuantity, total}) {
    return (
        <header className="shop-header row">
            <Link to="/" className="logo text-dark">ReStore</Link>
            <Link to="/cart" className="shopping-cart text-dark">
                <i className="cart-icon fa fa-shopping-cart" />
                {itemsQuantity} items (${total})
            </Link>
            
        </header>
    )
}

export default ShopHeader
