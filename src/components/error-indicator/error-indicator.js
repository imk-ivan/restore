import React from 'react'

function ErrorIndicator() {
    return (
        <div className="error-indicator">
            <p>Something goes wrong. Please, try again later.</p>
        </div>
    )
}

export default ErrorIndicator;
