import React from 'react'
import './book-item.css';

function BookItem({book, onAddedToCart}) {
    const { title, author, price, img} = book;
    console.log(price);
    return (
        <div className="book-item">
            <div className="book-cover">
                <img src={img} alt="cover"/>
            </div>
            <div className="book-details">
                <span className="book-title">{title}</span>
                <div className="book-author">{author}</div>
                <div className="book-price">${price.toFixed(2)}</div>
                <button 
                    className="btn btn-info add-to-cart"
                    onClick={onAddedToCart}>Add to cart</button>
            </div>
        </div>
    )
}

export default BookItem;
