import React from 'react'
import './shopping-cart-table.css';
import { connect } from 'react-redux';
import {itemAddedToCart, itemRemovedFromCart, allBooksRemovedFromCart} from '../../actions';

function ShoppingCartTable({
    items,
    total,
    onIncrease,
    onDecrease,
    onDelete
}) {
    const renderRow = (item, index) => {
        const {id,title, quantity, total} = item;
        return(
            <tr key={id}>
                <td>{index+1}</td>
                <td>{title}</td>
                <td>{quantity}</td>
                <td>${total.toFixed(2)}</td>
                <td className="item-actions"> 
                    <button 
                        className="btn btn-outline-success action-btn"
                        onClick={()=>onIncrease(id)}>
                            <i className="fa fa-plus-circle"/>
                    </button>
                    <button 
                        className="btn btn-outline-warning action-btn"
                        onClick={()=>onDecrease(id)}>
                            <i className="fa fa-minus-circle"/>
                    </button>
                    <button 
                        className="btn btn-outline-danger action-btn"
                        onClick={()=>onDelete(id)}>
                            <i className="fa fa-trash-o"/>
                    </button>
                </td>
        </tr>
        )
    }
    return (
        <div className='shopping-cart-table'>
            <h2>Your Order</h2>
            <table className="table">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Item</th>
                    <th>Count</th>
                    <th>Price</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        items.map(renderRow)
                    }
                </tbody>
            </table>
            <div className="cart-total">
                Total: ${total}
            </div>
        </div>
    )
}

const mapStateToProps = ({shoppingCart}) => {
    return{
        items: shoppingCart.cartItems,
        total: shoppingCart.total
    }
}

const mapDispatchToProps = {
    onIncrease: itemAddedToCart,
    onDecrease: itemRemovedFromCart,
    onDelete: allBooksRemovedFromCart,
}

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCartTable)
