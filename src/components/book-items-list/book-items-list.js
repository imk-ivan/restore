import React from 'react'
import { Component } from 'react'
import { connect } from 'react-redux';
import BookItem from '../book-item/book-item';
import { withBookstoreServiceContext } from '../hoc';
import {fetchBooks, itemAddedToCart} from '../../actions';
import {compose} from '../../utils';
import ErrorIndicator from '../error-indicator';

import './book-items-list.css';
import Spinner from '../spinner';

const BookList = ({books, onAddedToCart}) => {
    return (
        <div className="book-items-list">
            <ul>
                {
                    books.map(book => {
                        return <li key={book.id}>
                                <BookItem 
                                    book={book}
                                    onAddedToCart={()=>onAddedToCart(book.id)}/>
                            </li>
                    })
                }
            </ul>

        </div>
    )
}

class BookListContainer extends Component {

    componentDidMount(){
        const {
            fetchBooks,
         } = this.props;
         fetchBooks();     
    }

    render(){
        const { books, loading, error, onAddedToCart}  = this.props;

        if(loading){
            return <Spinner/>
        }
        if(error != null){
            return <ErrorIndicator/>
        }

        return <BookList books={books} onAddedToCart={onAddedToCart}/>
    }
}

const mapStateToProps = ({bookList}) => {
    return{
        books: bookList.books,
        loading: bookList.loading,
        error: bookList.error
    }
}

const mapDispatchToProps = (dispatch, ownProps) =>{
    return{
        fetchBooks: fetchBooks(ownProps.bookstoreService, dispatch),
        onAddedToCart: (id)=> {
            dispatch(itemAddedToCart(id))
        }
    }
}

export default compose(
    withBookstoreServiceContext(),
    connect(mapStateToProps, mapDispatchToProps)
)(BookListContainer);
