import React from 'react'
import { BookstoreServiceConsumer } from '../bookstore-service-context/bookstore-service-context'


const withBookstoreServiceContext = () => (View) =>{
    return(props) => (
        <BookstoreServiceConsumer>
            {
                (bookstoreService) => {
                    return <View {...props} bookstoreService={bookstoreService} />
                }
            }
        </BookstoreServiceConsumer>
    )
}

export default withBookstoreServiceContext;