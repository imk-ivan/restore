export default class BookstoreService {
    
    data = [
        {
            id: 1,
            title: 'The Adventures of Tom Sawyer',
            author: 'Mark Twain',
            price: 15.99,
            img: 'https://m.media-amazon.com/images/I/41nbsmV+fTL.jpg'
        },
        {
            id: 2, 
            title: 'Treasure Island', 
            author: 'Robert Louis Stevenson', 
            price: 36.00, 
            img: 'https://images-na.ssl-images-amazon.com/images/I/515x24zxu4L._SX381_BO1,204,203,200_.jpg'},
    ];

    getBooks() {
        return new Promise((resolve, reject)=>{
            setTimeout(()=>{
                if(Math.random() >= 0.75){
                    reject(new Error('test error'));
                }
                else{
                    resolve(this.data);
                }
                
            },700)
        })
    }
};
