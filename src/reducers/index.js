import updateBookList from './book-list';
import updateShoppingCart from './shopping-cart';

const initialState = {
    bookList: {
        books: [],
        loading: false,
        error: null,
    },
    shoppingCart: {
        cartItems: [],
        cartTotal: 0
    }
}

const reducer = (state = initialState, action) => {
    return{
        bookList: updateBookList(state, action),
        shoppingCart: updateShoppingCart(state, action)
    }
}

export default reducer;