const addOrUpdateItem = (array, item, idx) => {

    let items = [...array];
    
    if(item.quantity === 0){
        return [
            ...items.slice(0, idx),
            ...items.slice(idx+1)
        ]
    }

    if(idx > -1){
        items[idx] = item;
    }
    else{
        items.push(item)
    }
    return items;
}

const updateCartItem = (book, item = {}, quantityToUpdate = 1) => {
    const {
        id = book.id,
        title = book.title, 
        quantity = 0, 
        total = 0
    } = item;

    const newItem = {
        id,
        title,
        quantity: quantity + quantityToUpdate,
        total: total + quantityToUpdate * book.price,
    }

    return newItem;
}

const updateOrder = (state, itemId, quantityToUpdate = 1) => {
    const { bookList:{books}, shoppingCart:{cartItems} } = state;
    const book = books.find((book) => book.id === itemId);
    const itemIndex = cartItems.findIndex(item => item.id === itemId);
    const item = cartItems[itemIndex];
    const newItem = updateCartItem(book, item, quantityToUpdate);
    
    const items = addOrUpdateItem(cartItems, newItem, itemIndex);

    return {
        cartTotal: 0,
        cartItems: items,
    };

}



const updateShoppingCart = (state, action) => {
    switch(action.type) {
        case 'ITEM_ADDED_TO_CART':
            return updateOrder(state, action.payload, 1)
        case 'ITEM_REMOVED_FROM_CART':
            return updateOrder(state, action.payload, -1)
        case 'ALL_BOOKS_REMOVED_FROM_CART':
            const item = state.shoppingCart.cartItems.find(({id}) => id === action.payload);
            return updateOrder(state, action.payload, -item.quantity);
        default:
            return state.shoppingCart
    }
}

export default updateShoppingCart;