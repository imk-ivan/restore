const booksLoaded = (newBooks) => {
    return {
        type: 'FETCH_BOOKS_LOADED',
        payload: newBooks,
    }
}

const booksRequested = () =>{
    return{
        type:'FETCH_BOOKS_REQUEST'
    }
}

const booksRequestError = (error) => {
    return{
        type:'FETCH_BOOKS_FAILURE',
        payload: error
    }
}

const itemAddedToCart = (itemId) => {
    return{
        type: 'ITEM_ADDED_TO_CART',
        payload: itemId
    }
}

const itemRemovedFromCart = (itemId) => {
    return{
        type: 'ITEM_REMOVED_FROM_CART',
        payload: itemId
    }
}

const allBooksRemovedFromCart = (itemId) => {
    return{
        type: 'ALL_BOOKS_REMOVED_FROM_CART',
        payload: itemId
    }
}

const fetchBooks = (bookstoreService, dispatch) => () => {
    dispatch(booksRequested());

    bookstoreService
        .getBooks()
        .then((books)=>{
            dispatch(booksLoaded(books));
        })
        .catch((ex)=>{
            console.log(ex);
            dispatch(booksRequestError(ex));
        }); 
}

export{
    booksLoaded,
    booksRequested,
    booksRequestError,
    fetchBooks,
    itemAddedToCart,
    itemRemovedFromCart,
    allBooksRemovedFromCart
}